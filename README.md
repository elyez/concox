# Elyez - Concox

[![build status](https://gitlab.com/elyez/concox/badges/master/build.svg)](https://gitlab.com/elyez/app/commits/master)

## Example

`'app.js'`
```
const Elyez = require('@elyez/gateway');
const Concox = require('@elyez/concox');

const app = Elyez();
app
  .set('encoding', 'hex')
  .set('parser', Concox.GT06N)
  .listen(PORT);
```


## Packages

* GT06 / GT06N
* GT02 / GT02D


## TODO

* Sending Command


## License

The MIT License (MIT)